var http = require("http")
const rabbitmqConnection = require('./connection')
const rabbitmqReader = require('./queue/read')

const QUEUE_NAME = 'myqueue'
const URL = 'amqp://localhost:5672/'
const USERNAME = 'myuser'
const PASSWORD = 'mypassword'

const port = 4041
let server = http.createServer(function (request, response) {})
rabbitmqConnection.connect(URL, USERNAME, PASSWORD)
setTimeout(function () {
  server.listen(port, function () {
    console.log(`listening on port ${port}`)
    rabbitmqReader.consume(QUEUE_NAME)
  })
}, 3000)
